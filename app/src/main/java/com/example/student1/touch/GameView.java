package com.example.student1.touch;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

/**
 * Created by student1 on 05.12.16.
 */

public class GameView extends View {
    private int x1, y1, x2, y2;
    private float touchX,touchY;
    private int x, y, size;
    private float xspeed;
    private float yspeed;
    private Bitmap pic;

    public void setIsMoving(boolean isMoving){
      this.isMoving = isMoving;
    }



    private boolean isMoving;

    public GameView (Context context) {
        super(context);
        pic = BitmapFactory.decodeResource(context.getResources(), R.drawable.cat );
        size = 200;
        xspeed = 5;
        yspeed = 5;
    }
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        Rect sourse = new Rect (0,0, pic.getWidth(), pic.getHeight());
        Rect rect =  new Rect (x - size, y-size, x+size, y+size);
        canvas.drawRect( x - size , y - size, x + size, y + size, new Paint() );
        canvas.drawBitmap(pic, sourse, rect, new Paint() );
        x += xspeed;
        y += yspeed;
        if ( x > canvas.getWidth() || x<0) xspeed = -xspeed;
        if ( y > canvas.getWidth() || y<0) yspeed = -yspeed;

        invalidate();
    }
    public void setTouchCoords(float x, float y){


       invalidate();

    }

}
